/* Dooba SDK
 * Skunk Bootloader
 */

#ifndef __KIWI_H
#define	__KIWI_H

// External Includes
#include <avr/io.h>
#include <inttypes.h>
#include <limits.h>

// Baud Rate
#define	KIWI_RATE															19200UL
#define	KIWI_PRESCALE														((F_CPU / (KIWI_RATE * 16UL)) - 1)

// Page Size
#define	KIWI_PGSIZE															256L

// Watchdog Values (On = 4 Seconds, Now = 16 mSeconds)
#define	KIWI_WD_ON															(_BV(WDP3) | _BV(WDE))
#define	KIWI_WD_NOW															(_BV(WDP2) | _BV(WDE))
#define	KIWI_WD_OFF															(0)

// Watchdog Shortcuts
#define	kiwi_wd_rst()														asm volatile("wdr\n")
#define	kiwi_wd_set(v)														WDTCSR = _BV(WDCE) | _BV(WDE); WDTCSR = v

// LED
#define	KIWI_LED_DDR														DDRD
#define	KIWI_LED_PORT														PORTD
#define	KIWI_LED_P															5

// LED Shortcuts
#define	kiwi_led_on()														KIWI_LED_PORT |= _BV(KIWI_LED_P)
#define	kiwi_led_off()														KIWI_LED_PORT &= ~(_BV(KIWI_LED_P))

// Commands
#define	KIWI_CMD_SET_ADDR													0x00
#define	KIWI_CMD_GET_INFO													0x01
#define	KIWI_CMD_SET_LFUSE													0x02
#define	KIWI_CMD_SET_HFUSE													0x03
#define	KIWI_CMD_SET_EFUSE													0x04
#define	KIWI_CMD_SET_LOCKB													0x05
#define	KIWI_CMD_PROG_PAGE													0x06
#define	KIWI_CMD_DONE														0x07

// Responses
#define	KIWI_RES_OK															0xaa

// Application Address
#define	kiwi_app															(0)

// SPM Control
#define	kiwi_spm_busy()														(SPMCSR & (uint8_t)_BV(SPMEN))
#define	kiwi_spm_wait()														while(kiwi_spm_busy()) { /* NoOp */ }

// SPM Shortcuts
#define	KIWI_ERASE_PAGE														(_BV(SPMEN) | _BV(PGERS))
#define	KIWI_WRITE_PAGE														(_BV(SPMEN) | _BV(PGWRT))
#define	KIWI_FILL_PAGE														(_BV(SPMEN))
#define	KIWI_ENABLE_RWW														(_BV(SPMEN) | _BV(RWWSRE))

// Page Commands
#define	kiwi_fill_page(addr, data)\
(__extension__({\
	asm volatile\
	(\
		"movw  r0, %3\n\t"\
		"out %0, %1\n\t"\
		"spm\n\t"\
		"clr  r1\n\t"\
		:\
		: "i" (_SFR_IO_ADDR(SPMCSR)),\
		  "r" ((uint8_t)KIWI_FILL_PAGE),\
		  "z" ((uint16_t)addr),\
		  "r" ((uint16_t)data)\
		: "r0"\
	);\
}))
#define	kiwi_erase_page(addr)\
(__extension__({\
	asm volatile\
	(\
		"out %0, %1\n\t"\
		"spm\n\t"\
		:\
		: "i" (_SFR_IO_ADDR(SPMCSR)),\
		  "r" ((uint8_t)KIWI_ERASE_PAGE),\
		  "z" ((uint16_t)addr)\
	);\
}))
#define	kiwi_write_page(addr)\
(__extension__({\
	asm volatile\
	(\
		"out %0, %1\n\t"\
		"spm\n\t"\
		:\
		: "i" (_SFR_IO_ADDR(SPMCSR)),\
		  "r" ((uint8_t)KIWI_WRITE_PAGE),\
		  "z" ((uint16_t)addr)\
	);\
}))

// Enable RWW
#define	kiwi_enable_rww()\
(__extension__({\
	asm volatile\
	(\
		"sts %0, %1\n\t"\
		"spm\n\t"\
		:\
		: "i" (_SFR_MEM_ADDR(SPMCSR)),\
		  "r" ((uint8_t)KIWI_ENABLE_RWW)\
	);\
}))

// Start Application
extern void kiwi_start_app(uint8_t rst_flags) __attribute__((naked));

#endif
